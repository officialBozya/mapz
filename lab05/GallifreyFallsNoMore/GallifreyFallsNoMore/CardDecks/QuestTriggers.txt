[{
	"ImagePath":"Assets/Cards/Romana_2.png",
	"Text":"In we have developed a new weapon, but it is a little strange...",
	"Yes":"Show me this weapon.",
	"No":"Give it to the general.",
	"InfluenceYes":
	{
		"Science":0,
		"Time":0,
		"MilitaryPower":0,
		"Money":0
	},
	"InfluenceNo":
	{
		"Science":0,
		"Time":0,
		"MilitaryPower":0.2,
		"Money":0
	},
	"QuestName":"BadWolf"
}]