﻿using GallifreyFallsNoMore.Model;
using GallifreyFallsNoMore.Model.Serialize;
using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using GallifreyFallsNoMore.Model.Collections;
using GallifreyFallsNoMore.Model.Buffs;

namespace GallifreyFallsNoMore.ViewModel
{
    internal class QuestMode : PlayingMode
    {
        private string _questName;
        private CardsCollection _cardsCollection;
        private QuestIterator _questIterator;
        public QuestMode(string questName, PlayingFieldViewModel playingFieldViewModel)
        {
            _playingFieldViewModel = playingFieldViewModel;
            _questName = questName;
            _cardsCollection = new CardsCollection(questName, PackType.Quest);
            _questIterator = (QuestIterator)_cardsCollection.GetEnumerator();
            if (_questIterator.MoveNext())
            {
                _playingFieldViewModel.Cards.Add((Card)_questIterator.Current());
                _playingFieldViewModel.Cards.Add(Card.GetJacket());   
            }
        }
        public override void CardChange()
        {
            _questIterator.Answer = _playingFieldViewModel.Answer;
            if (_questIterator.MoveNext())
            {
                _playingFieldViewModel.Cards.Add((Card)_questIterator.Current());
                _playingFieldViewModel.Cards.Add(Card.GetJacket());
            }
            else
            {
                var buff = GetBuff();
                if (buff != null)
                {
                    _playingFieldViewModel.Buffs.Add(buff);
                    buff.SetBuff(_playingFieldViewModel);
                }
                _playingFieldViewModel.BasicIterator.MoveNext();
                _playingFieldViewModel.Cards.Add((Card)_playingFieldViewModel.BasicIterator.Current());
                _playingFieldViewModel.Cards.Add(Card.GetJacket());
                _playingFieldViewModel.TransitionTo(new NormalMode());
            }
        }

        public override void YearChange()
        {
        }

        public override void YearsChanged()
        {
        }
        private Buff GetBuff()
        {
            switch (_questName)
            {
                case "BadWolf":
                    return new BadWolfBuff();
                case "Coin":
                    return new CoinBuff();
                case "Moment":
                    return new MomentBuff();
                case "SonicScrewdriwer":
                    return new SonicScrewdriverBuff();
                case "WarTardis":
                    return new WarTardisBuff();
                case "tardis":
                    return new TardisBuff();
                default:
                    return null;
            }
        }
    }
}
