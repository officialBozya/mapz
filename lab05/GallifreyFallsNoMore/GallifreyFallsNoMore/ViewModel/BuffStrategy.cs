﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.ViewModel
{
    class BuffStrategy : IStrategy
    {
        public string InfluenceAlgorithm(double stat)
        {
            switch (stat)
            {
                case 0.1:
                    return "→";
                case 0.2:
                    return "⇉";
                case -0.1:
                    return "←";
                case -0.2:
                    return "⇇";
                default:
                    return "";
            }
        }
    }
}
