﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.Model.Collections
{
    class BasicIterator : Iterator
    {
        private CardsCollection _collection;
        private int _position = -1;
        public BasicIterator(CardsCollection collection)
        {
            this._collection = collection;
        }
        public override object Current()
        {
            return this._collection.GetItems()[_position];
        }

        public override int Key()
        {
            return this._position;
        }

        public override bool MoveNext()
        {
            var randomizer = new Random();
            int updatedPosition = randomizer.Next(_collection.GetItems().Count);
            if (updatedPosition == _position)
            {
                MoveNext();
            }
            _position = updatedPosition;
            return true;
        }

        public override void Reset()
        {
            _position = 0;
        }
    }
}
