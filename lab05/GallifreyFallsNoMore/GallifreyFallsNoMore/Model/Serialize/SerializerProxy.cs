﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace GallifreyFallsNoMore.Model.Serialize
{
    internal class SerializerProxy
    {
        public void Serialize(List<Card> cards, string packName)
        {
            CardSerializer serializer = new CardSerializer(new Serializer<Card>());
            var items = serializer.Deserialize(packName);
            items.AddRange(cards);
            serializer.Serialize(items, packName);
            Log(true, packName, items.Count);
        }
        public void Serialize(Card card, string packName)
        {
            this.Serialize(new List<Card> { card }, packName);
        }
        public List<Card> Deserialize(string packName)
        {
            CardSerializer serializer = new CardSerializer(new Serializer<Card>());
            var items = serializer.Deserialize(packName);
            Log(false, packName, items.Count);
            return items;
        }
        public void Serialize(List<CardQuestTrigger> cards)
        {
            var serializer = new QuestTriggerSerializer(new Serializer<CardQuestTrigger>());
            var items = serializer.Deserialize("QuestTriggers");
            items.AddRange(cards);
            serializer.Serialize(items, "QuestTriggers");
            Log(true, "QuestTriggers", items.Count);
        }
        public void Serialize(CardQuestTrigger card)
        {
            this.Serialize(new List<CardQuestTrigger>() { card });
        }
        public List<CardQuestTrigger> DeserializeQuestTriggers()
        {
            var serializer = new QuestTriggerSerializer(new Serializer<CardQuestTrigger>());
            var items = serializer.Deserialize("QuestTriggers");
            Log(false, "QuestTriggers", items.Count);
            return items;
        }
        public void Serialize(List<LeaderboardField> fields)
        {
            LeaderboardSerializer serializer = new LeaderboardSerializer(new Serializer<LeaderboardField>());
            var items = serializer.Deserialize();
            foreach(var field in fields)
            {
                field.ID = items.Count;
                items.Add(field);
            }
            serializer.Serialize(items);
            Log(true, "leaderboard", items.Count);
        }
        public void Serialize(LeaderboardField field)
        {
            this.Serialize(new List<LeaderboardField> { field });
        }
        public List<LeaderboardField> DeserializeLeaderboard()
        {
            LeaderboardSerializer serializer = new LeaderboardSerializer(new Serializer<LeaderboardField>());
            var items = serializer.Deserialize();
            Log(false, "leaderboard", items.Count);
            return items;
        }
        public void Serialize(List<string> decks)
        {
            DecksSerializer serializer = new DecksSerializer(new Serializer<string>());
            var items = serializer.Deserialize();
            items.AddRange(decks);
            serializer.Serialize(items);
            Log(true, "decks", items.Count);
        }
        public void Serialize(string deck)
        {
            this.Serialize(new List<string> { deck });
        }
        public List<string> DeserializeDecks() 
        {
            DecksSerializer serializer = new DecksSerializer(new Serializer<string>());
            var items = serializer.Deserialize();
            Log(false, "decks", items.Count);
            return items;
        }
        private void Log(bool serialize, string path, int items)
        {
            string _path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Log.txt");
            StreamWriter streamWriter = new StreamWriter(_path, true);
            string message;
            if (serialize)
            {
                message = "Serialized " + items + " into " + path + " at " + DateTime.Now.ToString();
            }
            else
            {
                message = "Deserialized " + items + " from " + path + " at " + DateTime.Now.ToString();
            }
            streamWriter.WriteLine(message);
            streamWriter.Close();
        }
    }
}
