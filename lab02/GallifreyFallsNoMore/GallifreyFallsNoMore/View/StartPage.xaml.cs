﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using GallifreyFallsNoMore.ViewModel;

namespace GallifreyFallsNoMore.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StartPage : ContentPage
    {
        StartPage()
        {
            InitializeComponent();
            IAbstractFactoryViewModel factoryViewModel = new ViewModelFactory();
            BindingContext = factoryViewModel.CreateStart(this.Navigation);
            TryAgain = false;
        }
        public bool TryAgain { get; set; }
        private static StartPage _instance = null;
        private static readonly object _lock = new object();
        public static StartPage GetPage()
        {
            if (_instance == null)
            {
                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = new StartPage();
                    }
                }
            }
            return _instance;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (TryAgain)
            {
                TryAgain = false;
                Navigation.PushModalAsync(new PlayingFieldPage());
            }
        }
    }
}