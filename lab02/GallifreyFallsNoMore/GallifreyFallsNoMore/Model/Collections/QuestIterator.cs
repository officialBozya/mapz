﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.Model.Collections
{
    public enum Answer
    {
        Yes = -1,
        No = 0
    }
    class QuestIterator : Iterator
    {
        private CardsCollection _collection;
        private int _position = -1;
        private int _positionInQuest = 0;
        private Answer _answer = Answer.No;
        public QuestIterator(CardsCollection collection)
        {
            this._collection = collection;
        }

        public Answer Answer { get => _answer; set => _answer = value; }

        public override object Current()
        {
            return this._collection.GetItems()[_position];
        }

        public override int Key()
        {
            return this._position;
        }

        public override bool MoveNext()
        {
            int updatedPosition = _positionInQuest * 2 + (int)Answer;
            _positionInQuest++;
            if (updatedPosition >= 0 && updatedPosition < this._collection.GetItems().Count)
            {
                this._position = updatedPosition;
                return true;
            }
            else
            {
                return false;
            }
        }

        public override void Reset()
        {
            _position = 0;
            _positionInQuest = 0;
        }
    }
}
