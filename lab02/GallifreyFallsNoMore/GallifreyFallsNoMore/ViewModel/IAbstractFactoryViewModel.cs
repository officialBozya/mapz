﻿using MLToolkit.Forms.SwipeCardView;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace GallifreyFallsNoMore.ViewModel
{
    interface IAbstractFactoryViewModel
    {
        BaseViewModel CreateStart(INavigation navigation);
        BaseViewModel CreateLeaderboard(INavigation navigation);
        BaseViewModel CreatePlayingField(INavigation navigation, SwipeCardView swipeCardView);
        BaseViewModel CreateGameEnd(INavigation navigation);
    }
}
