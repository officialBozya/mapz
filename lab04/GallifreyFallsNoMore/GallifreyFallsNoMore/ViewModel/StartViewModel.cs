﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using GallifreyFallsNoMore.Model;
using Xamarin.Forms;
using System.Windows.Input;
using GallifreyFallsNoMore.View;

namespace GallifreyFallsNoMore.ViewModel
{
    class StartViewModel: BaseViewModel
    {
        public ICommand GoPlayingFieldCommand { private set; get; }
        public ICommand GoLeaderboardCommand { private set; get; }
        public StartViewModel(INavigation navigation) : base(navigation)
        {
            GoPlayingFieldCommand = new Command(GoPlayingField);
            GoLeaderboardCommand = new Command(GoLeaderboard);
        }
        private void GoPlayingField()
        {
            Navigation.PushModalAsync(new PlayingFieldPage());
        }
        private void GoLeaderboard()
        {
            Navigation.PushModalAsync(LeaderboardPage.GetPage());
        }
    }
}
