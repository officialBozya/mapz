﻿using GallifreyFallsNoMore.Model;
using GallifreyFallsNoMore.Model.Serialize;
using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.ViewModel.DeathCoR
{
    class DefaultDeathHandler : IDeathHandler
    {
        protected IDeathHandler _next;
        public virtual bool DeathHandle(object request)
        {
            if (this._next != null)
            {
                return this._next.DeathHandle(request);
            }
            else
            {
                var field = request as PlayingFieldViewModel;
                field.GameEnded = true;
                LeaderboardField corpse = new LeaderboardField() { DeathReason = field.DeathCousedBy(), YearsLived = field.Player.Years };
                SerializerProxy serializer = new SerializerProxy();
                serializer.Serialize(corpse);
                return true;
            }
        }

        public IDeathHandler SetNext(IDeathHandler handler)
        {
            this._next = handler;
            return handler;
        }
    }
}
