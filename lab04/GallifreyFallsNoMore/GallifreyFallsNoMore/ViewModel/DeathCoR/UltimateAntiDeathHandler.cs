﻿using GallifreyFallsNoMore.Model.Buffs;
using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.ViewModel.DeathCoR
{
    class UltimateAntiDeathHandler : DefaultDeathHandler
    {
        public override bool DeathHandle(object request)
        {
            var field = request as PlayingFieldViewModel;
            if (field.DeathCalcualte())
            {
                field.MilitaryPower = 0.5;
                field.Science = 0.5;
                field.Time = 0.5;
                field.Money = 0.5;
                for (int i = 0; i < field.Player.Buffs.Count; i++)
                {
                    Buff buff = field.Player.Buffs[i];
                    if (buff is TardisBuff)
                        field.Player.Buffs.Remove(buff);
                }
                field.DeathHandler = new DefaultDeathHandler();
                foreach (var buff in field.Player.Buffs)
                {
                    buff.SetBuff(field);
                }
            }
            return false;
        }
    }
}
