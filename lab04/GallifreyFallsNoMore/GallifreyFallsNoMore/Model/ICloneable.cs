﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.Model
{
    public interface ICloneable
    {
        ICloneable Clone();
    }
}
