﻿using System;
using System.Collections.Generic;
using System.Text;
using Faker;
using GallifreyFallsNoMore.ViewModel;

namespace GallifreyFallsNoMore.Model.Buffs
{
    class BadWolfBuff : Buff
    {
        public BadWolfBuff()
        {
            ImagePath = "Assets/Buffs/BadWolfTardis.png";
            Name = "BadWolf";
        }
        public override void SetBuff(object sender)
        {
            var field = sender as PlayingFieldViewModel;
            field.SetStrategy(new BuffStrategy());
        }
    }
}
