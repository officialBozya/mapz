﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.Model.Buffs
{
    abstract class Buff
    {
        public string Name { get; protected set; }
        public string ImagePath { get; protected set; }
        public abstract void SetBuff(object sender);
    }
}
