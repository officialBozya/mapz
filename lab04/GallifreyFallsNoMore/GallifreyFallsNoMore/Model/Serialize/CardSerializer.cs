﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace GallifreyFallsNoMore.Model.Serialize
{
    internal class CardSerializer : SerializersDecorator<Card>
    {
        public CardSerializer(AbstractSerializer<Card> serializer) : base(serializer) { }
        public override List<Card> Deserialize(string packName)
        {
            return base.Deserialize("GallifreyFallsNoMore.CardDecks." + packName+".txt");
        }

        public override void Serialize(List<Card> items, string packName)
        {
            base.Serialize(items, "GallifreyFallsNoMore.CardDecks." + packName + ".txt");
        }
    }
}
