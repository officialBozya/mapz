﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using System.Reflection;

namespace GallifreyFallsNoMore.Model.Serialize
{
    internal class Serializer<T> : AbstractSerializer<T>
    {
        public override void Serialize(List<T> items, string packName)
        {
            var serializer = new JsonSerializer();
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), packName + ".txt");
            using (JsonWriter writer = new JsonTextWriter(new StreamWriter(path)))
            {
                serializer.Serialize(writer, items);
            }
        }

        public override List<T> Deserialize(string packName)
        {
            var serializer = new JsonSerializer();
            List<T> items;
            Stream stream = null;
            if (packName.EndsWith(".txt"))
            {
                var assembly = IntrospectionExtensions.GetTypeInfo(typeof(Serializer<T>)).Assembly;
                stream = assembly.GetManifestResourceStream(packName);
            }
            else
            {
                var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), packName + ".txt");
                if (!File.Exists(path))
                {
                    return new List<T>();
                }
                stream = new FileStream(path,FileMode.Open);
            }
            using (JsonReader reader = new JsonTextReader(new StreamReader(stream)))
            {
                items = serializer.Deserialize<List<T>>(reader);
            }
            return items;
        }
    }
}
