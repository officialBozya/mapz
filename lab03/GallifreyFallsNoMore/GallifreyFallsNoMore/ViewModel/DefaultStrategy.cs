﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.ViewModel
{
    class DefaultStrategy : IStrategy
    {
        public string InfluenceAlgorithm(double stat)
        {
            if (Math.Abs(stat) == 0.1)
            {
                return "·";
            }
            if (Math.Abs(stat) == 0.2)
            {
                return "•";
            }
            return "";
        }
    }
}
