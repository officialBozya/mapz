﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.Model.Serialize
{
    internal class QuestTriggerSerializer : SerializersDecorator<CardQuestTrigger>
    {
        public QuestTriggerSerializer(AbstractSerializer<CardQuestTrigger> serializer) : base(serializer) { }
        public override List<CardQuestTrigger> Deserialize(string packName)
        {
            return base.Deserialize(packName);
        }

        public override void Serialize(List<CardQuestTrigger> items, string packName)
        {
            base.Serialize(items, packName);
        }
    }
}
