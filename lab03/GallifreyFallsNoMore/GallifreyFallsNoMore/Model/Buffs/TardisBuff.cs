﻿using System;
using System.Collections.Generic;
using System.Text;
using GallifreyFallsNoMore.ViewModel.DeathCoR;

namespace GallifreyFallsNoMore.Model.Buffs
{
    class TardisBuff : AntiDeathBuff
    {
        public TardisBuff()
        {
            ImagePath = "Assets/Buffs/Tardis.png";
            _deathHandler = new UltimateAntiDeathHandler();
            Name = "";
        }
    }
}
