﻿using System;
using System.Collections.Generic;
using System.Text;
using GallifreyFallsNoMore.ViewModel.DeathCoR;

namespace GallifreyFallsNoMore.Model.Buffs
{
    class SonicScrewdriverBuff : AntiDeathBuff
    {
        public SonicScrewdriverBuff()
        {
            ImagePath = "Assets/Buffs/SonicScrewdrivers.png";
            _deathHandler = new ScienceAntiDeathHandler();
            Name = "";
        }
    }
}
