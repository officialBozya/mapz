﻿using GallifreyFallsNoMore.Model.Serialize;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace GallifreyFallsNoMore.Model.Collections
{
    enum PackType
    {
        Basic = 0,
        Quest = 1,
        QuestTrigger = 2
    }
    class CardsCollection : IteratorAggregate
    {
        PackType _packType;
        public CardsCollection(string packName, PackType packType)
        {
            var serializer = new SerializerProxy();
            if (packType == PackType.QuestTrigger)
            {
                _cards = new List<Card>();
                foreach (var card in serializer.DeserializeQuestTriggers())
                {
                    _cards.Add(card);
                }
            }
            else
            {
                _cards = serializer.Deserialize(packName);
            }
            _packType = packType;
        }
        List<Card> _cards = new List<Card>();
        public List<Card> GetItems()
        {
            return _cards;
        }
        public void AddItem(Card item)
        {
            _cards.Add(item);
        }
        public override IEnumerator GetEnumerator()
        {
            switch (_packType)
            {
                case PackType.Basic:
                    return new BasicIterator(this);
                case PackType.Quest:
                    return new QuestIterator(this);
                case PackType.QuestTrigger:
                    return new QuestTriggerIterator(this);
                default:
                    return null;
            }
        }
    }
}
