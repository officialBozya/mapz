﻿using GallifreyFallsNoMore.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GallifreyFallsNoMore.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LeaderboardPage : ContentPage
    {
        LeaderboardPage()
        {
            InitializeComponent();
            IAbstractFactoryViewModel factoryViewModel = new ViewModelFactory();
            BindingContext = factoryViewModel.CreateLeaderboard(this.Navigation);
        }
        private static LeaderboardPage _instance = null;
        private static readonly object _lock = new object();
        public static LeaderboardPage GetPage()
        {
            if (_instance == null)
            {
                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = new LeaderboardPage();
                    }
                }
            }
            return _instance;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            ((LeaderboardViewModel)(BindingContext)).AppearingCommand.Execute(null);
        }
    }
}