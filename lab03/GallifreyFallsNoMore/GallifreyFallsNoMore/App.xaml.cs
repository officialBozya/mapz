﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using GallifreyFallsNoMore.View;

namespace GallifreyFallsNoMore
{
    public partial class App : Application
    {
        public static double ScreenHeight;
        public static double ScreenWidth;
        public App()
        {
            InitializeComponent();

            MainPage = StartPage.GetPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
