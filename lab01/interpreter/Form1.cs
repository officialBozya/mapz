﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;
using interpreter.Expressions;

namespace interpreter
{
    public partial class Form1 : Form
    {
        Context context;
        Interpreter interpreter;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            context = new Context(textBox2, treeView1);
            interpreter = new Interpreter();
            interpreter.Execute(textBox1.Text, context);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox2.Clear();
            treeView1.BeginUpdate();
            treeView1.Nodes.Clear();
            treeView1.EndUpdate();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string filename = "info.txt";
            System.IO.File.WriteAllText(filename, Properties.Resources.info);
            Process.Start(filename);
        }
    }
}
