﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interpreter.Expressions
{
    abstract class BinaryExpression: UnaryExpression
    {
        public IExpression Op2 { get; set; }
    }
}
