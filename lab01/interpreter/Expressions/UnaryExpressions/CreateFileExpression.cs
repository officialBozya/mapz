﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace interpreter.Expressions.UnaryExpressions
{
    class CreateFileExpression: UnaryExpression
    {
        public override int Priority { get { return 0; } }
        public override dynamic Solve(Context context)
        {
            var fileName = context.Memory[Op1.Solve(context)];
            FileInfo f = new FileInfo(fileName);
            if (f == null)
            {
                context.Output.Text += "File is not created.\r\n";
                return null;
            }
            if (f.Exists)
            {
                context.Output.Text += "File exist.\r\n";
                return null;
            }
            var file = f.Create();
            file.Close();
            context.Output.Text += "File " + fileName + " is created.\r\n";
            return null;
        }
    }
}
