﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interpreter.Expressions
{
    class QuotesExpression : IExpression
    {
        public QuotesExpression(string _name)
        {
            name = _name;
        }
        private string name;
        public int Priority { get { return 4; } }
        public dynamic Solve(Context context)
        {
            if (!context.Memory.Keys.Contains("buffer"))
            {
                context.Memory.Add("buffer", "");
            }
            context.Memory["buffer"] = context.Quotes[name];
            return "buffer";
        }
    }
}
