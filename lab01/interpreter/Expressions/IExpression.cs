﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interpreter.Expressions
{
    interface IExpression
    {
        int Priority { get; }
        dynamic Solve(Context context);
    }
}
