﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interpreter.Expressions
{
    abstract class UnaryExpression: IExpression
    {
        public IExpression Op1 { get; set; }
        abstract public int Priority { get; }
        abstract public dynamic Solve(Context context);
    }
}
