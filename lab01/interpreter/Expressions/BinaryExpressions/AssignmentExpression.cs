﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interpreter.Expressions.BinaryExpressions
{
    class AssignmentExpression: BinaryExpression
    {
        public override int Priority { get { return 1; } }
        public override dynamic Solve(Context context)
        {
            var op1 = Op1.Solve(context);
            var op2 = Op2.Solve(context);
            context.Memory[op1] = context.Memory[op2];
            return op2;
        }
    }
}
