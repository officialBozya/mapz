﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace interpreter.Expressions.BinaryExpressions
{
    class MoveFileExpression : BinaryExpression
    {
        public override int Priority { get { return 0; } }
        public override dynamic Solve(Context context)
        {
            string path = context.Memory[Op1.Solve(context)];
            string newPath = context.Memory[Op2.Solve(context)];

            FileInfo fileInf = new FileInfo(path);
            if (fileInf.Exists)
            {
                fileInf.MoveTo(newPath);
                context.Output.Text += "File is moved to " + newPath + " .\r\n";
            }
            else 
            {
                context.Output.Text += "File is not exist.\r\n";
            }
            return null;
        }
    }
}
